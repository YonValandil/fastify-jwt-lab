const fp = require('fastify-plugin')

module.exports = fp(async function (fastify, opts) {
  fastify.register(require('fastify-jwt'), {
    secret: 'secret'
  })

  fastify.decorate('authenticate', async function (request, reply) {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  })

  // fastify
  //   .decorate('verifyJWTandLevel', function (request, reply, done) {
  //   // your validation logic
  //     done() // pass an error if the authentication fails
  //   })
  //   .decorate('verifyUserAndPassword', function (request, reply, done) {
  //   // your validation logic
  //     done() // pass an error if the authentication fails
  //   })
  //   .register(require('fastify-auth'))
  //   .after(() => {
  //     fastify.route({
  //       method: 'POST',
  //       url: '/auth-multiple',
  //       preHandler: fastify.auth([
  //         fastify.verifyJWTandLevel,
  //         fastify.verifyUserAndPassword
  //       ]),
  //       handler: (req, reply) => {
  //         req.log.info('Auth route')
  //         reply.send({ hello: 'world' })
  //       }
  //     })
  //   })
})
