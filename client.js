const axios = require('axios');

(async () => {
  const credentials = {
    foo: 'bar'
  }

  const client = axios.create({
    baseURL: 'http://127.0.0.1:3000'
  })

  // Middleware for the refresh token
  client.interceptors.response.use(
    success => {
      console.log('----- success')
      return success
    },
    async error => {
      console.log('----- error')
      const originalRequest = error.config
      if (
        error.response.status === 401 &&
          originalRequest.url.includes('/token')
      ) {
        return Promise.reject(error)
      } else if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true

        // setup a new valid token
        const token = await client.post('/token', credentials)
        console.log('refresh token => ', token.data.token)
        client.defaults.headers.common.Authorization = 'Bearer ' + token.data.token
        originalRequest.headers.Authorization = 'Bearer ' + token.data.token

        return client(originalRequest)
      }
      return Promise.reject(error)
    }
  )

  // Get TOKEN
  try {
    const token = await client.post('/token', credentials)
    console.log('token => ', token.data.token)
    client.defaults.headers.common.Authorization = 'Bearer ' + token.data.token
  } catch (error) {
    console.error(error)
  }

  // // Test route: /hello
  const req = await client.get('/hello').catch(err => console.error(err))
  console.log('req => ', req.data)

  // Same request but after 6s for testing axios interceptor refresh token
  setTimeout(async () => {
    console.log('6secondes passed !')
    const req = await client.get('/hello').catch(err => console.error(err))
    console.log('req new token => ', req.data)
  }, 6000)
})().catch(err => console.error(err))
