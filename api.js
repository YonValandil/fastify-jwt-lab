const fastify = require('fastify')()
// const fastify = require('fastify')({
//   logger: true
// })

// Test route: /hello with authenticate (protected routes)
fastify.register(require('./token_plugin'))
fastify.register(require('./hello_route'))

// Works but for all requests (even /token) so it's impossible to use
// fastify.addHook('onRequest', async (request, reply) => {
//   try {
//     await request.jwtVerify()
//   } catch (err) {
//     reply.send(err)
//   }
// })

fastify.post('/token', async (req, reply) => {
  const payload = req.body
  console.log('req => ', payload)
  const token = await fastify.jwt.sign(payload, { expiresIn: '5000' }) // '7d'
  reply.send({ token })
})

const start = async () => {
  try {
    const address = await fastify.listen(3000)
    fastify.log.info(`server listening on ${address}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()
