module.exports = async function (fastify, opts) {
  fastify.get(
    '/hello',
    {
      preValidation: [fastify.authenticate]
    },
    async function (request, reply) {
      // return request.user
      return { hello: 'world' }
    }
  )
}
